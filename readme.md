# Employee API

### See "worklog.md" for updates/work log/progress.

**Check the code for comments/details!**  
  
## HOW IT WORKS:  
The app simulates the usage of controller methods, through the
Postman application. The project boots locally.  

http://localhost:8080/employees - Is the main page for this app.
Through Postman we can use these requests as JSON, structure must be matched for
edits to be accepted. If not, you will get an error.  

For adding and replacing an Employee Post the unique employee-code is validated as a 
Luhn-code through a validator, if an invalid Luhn code is entered the request will fail.

**GET Request the full hashmap of Employees**  
http://localhost:8080/employees  
*Reads the current hashmap of Employees.*  

**GET Request a specific employee by key id:**   
http://localhost:8080/employees/ (insert id number here)  
*Gets the specific entered id through url, enter a number to read a specific employee data.* 

**GET Request the full names of employees and sum of devices:**   
http://localhost:8080/employees/devices/summary  
*Gets every employee and their respective amount of used devices.*

**GET Request "New" Employees (Last Two Years), full name and employee-code:**   
http://localhost:8080/employees/new
*Gets latest 2 years employed full-name + employee code as a String.*


**Get Request a list of all employees and their birthdays:**   
http://localhost:8080/employees/birthdays
*Gets every employee names and their respective birthdays, and days until that day!*



**POST Create an employee post:**  
http://localhost:8080/employees  
Create an employee post with the same structure as the examples, structure must match. A key value is auto-generated.
*See the first GET request for examples regarding the data structure.*

**NOTE: employeeCode must be a valid Luhn code, if not - it is not validated and therefore not accepted!**

**PUT Replace an employee post:**  
http://localhost:8080/employees (targeted key number to replace)  
*Targets the url id to replace, finds the employee and replaces it with the new values,
structure must match the examples to be accepted.*   

**NOTE: employeeCode must be a valid Luhn code, if not - it is not validated and therefore not accepted!**

**PATCH Modify an employee post:**  
http://localhost:8080/employees/ (Insert id number here)  
*Targets the entered url id to be modified, values must match the pattern of examples to be accepted.*

**DELETE an employee post:**
http://localhost:8080/employees/ (Insert target id here)    
*Deletes targeted id through url, if it exists.*


### About: 
A "bigger" assignment with the mission of creating an Employee API, being able to track "employees"-data. It puts our 
recent knowledge to test as we have to construct it with certain demands.



