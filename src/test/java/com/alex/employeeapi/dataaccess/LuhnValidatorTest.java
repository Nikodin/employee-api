package com.alex.employeeapi.dataaccess;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnValidatorTest {


    //Tests with a invalid code, should return false.
    @Test
    public void employeeCodeValidator_TestNonValidatedCode_ReturnFalse() {


        //Arrange
        String notValidCode = "2094778172";
        boolean expectedResponse = false;
        LuhnValidator luhnValidator = new LuhnValidator();

        //Act
        boolean actual = LuhnValidator.employeeCodeValidator(notValidCode);

        //Assert
        assertEquals(expectedResponse, actual);

    }

    //Tests with a valid generated code, should return true.
    @Test
    public void employeeCodeValidator_TestValidatedCode_ReturnTrue() {

        //Arrange
        String validCode = "8470396170";
        boolean expectedResponse = true;
        LuhnValidator luhnValidator = new LuhnValidator();

        //Act
        boolean actual = LuhnValidator.employeeCodeValidator(validCode);

        //Assert
        assertEquals(expectedResponse, actual);


    }


    //Tests the case for even entries, according to the Luhn validation.
    //Expected result was done by hand, from a generated valid code.
    @Test
    void evenNumbersCount_EnterValidLuhnCode_ReturnEvenSum() {
        //Arrange
        String validCode = "8470396170";
        int expectedResult = 26;
        LuhnValidator luhnValidator = new LuhnValidator();

        //Act
        int actual = LuhnValidator.evenNumbersCount(validCode);

        //Assert
        assertEquals(expectedResult, actual);


    }

    //Tests the case for odd entries, according to the Luhn validation.
    //Enter a long code > 10 digits, should fail.
    @Test
    void employeeCodeValidator_EnterLongCodeOver10_ReturnOddSum() {
        //Arrange
        String longCode = "84703961702";
        boolean expectedResult = false;
        LuhnValidator luhnValidator = new LuhnValidator();

        //Act
        boolean actual = LuhnValidator.employeeCodeValidator(longCode);

        //Assert
        assertEquals(expectedResult, actual);
    }

    //These (below) were done post-coding sadly
//Tests with a short code < 10 digits, should fail.
    @Test
    public void employeeCodeValidator_TestShortCodeUnder10_ReturnFalse() {


        //Arrange
        String notValidCode = "20947781";
        boolean expectedResponse = false;
        LuhnValidator luhnValidator = new LuhnValidator();

        //Act
        boolean actual = LuhnValidator.employeeCodeValidator(notValidCode);

        //Assert
        assertEquals(expectedResponse, actual);

    }
}