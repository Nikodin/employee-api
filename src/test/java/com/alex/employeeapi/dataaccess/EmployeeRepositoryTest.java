package com.alex.employeeapi.dataaccess;

import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.domain.EmploymentType;
import com.alex.employeeapi.models.domain.Position;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {

    //Check if repo is filled or not with startup variables.
    @Test
    void getAllEmployees_checkBootupRepoContent_ShouldReturnThreePosts() {
        //Arrange
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int expected = 3;

        //Act
        int actual = employeeRepository.getAllEmployees().size();

        //Assert
        assertEquals(expected, actual);
    }


    //Asks for a non-existent post = null.
    @Test
    void getEmployee_getNonExistingId_ShouldReturnNull() {
        //Arrange
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int getId = 10;
        Employee expected = null;

        //Act
        Employee actual = employeeRepository.getEmployee(getId);

        //Assert
        assertEquals(null, actual);
    }

    //Asks for an existing post, compares the required values, should be OK.
    @Test
    void getEmployee_matchDataWithStoredData_ShouldReturnMatch() {
        //Arrange
        EmployeeRepository employeeRepository = new EmployeeRepository();
        int getId = 2;
        Employee expected = new Employee(
                "1634751505",
                "Jane",
                "Doe",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{"Alienware X17", "Samsung 30-inch LED monitor"})));

        //Act
        Employee actual = employeeRepository.getEmployee(getId);

        //Assert
        assertEquals(expected, actual);
    }

    //Check if value is added, and that id is incremented by adding a post. Also valid Luhn-code, should be OK.
    @Test
    void createEmployee_createNewEmployee_ShouldMatchAndBeValid() {
        //Arrange
        EmployeeRepository employeeRepository = new EmployeeRepository();

        Employee expected = employeeRepository.createEmployee(new Employee(
                "6785530954",
                "Cole",
                "Train",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{"Alienware X17"}))));

        //Act
        Employee actual = employeeRepository.getEmployee(4);

        //Assert
        assertEquals(expected, actual);

    }

    //Replaces entire post with new one, should return replacement.
    @Test
    void replaceEmployee_replaceFirstExample_ShouldReturnTheNewReplacement() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        //Arrange
        Employee expected = employeeRepository.replaceEmployee(1, new Employee(
                "6785530954",
                "Cole",
                "Train",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{"Alienware X17"}))));

        //Act
        Employee actual = employeeRepository.getEmployee(1);

        //Assert
        assertEquals(expected, actual);
    }

    //Modifies startup post to the new template below, should return same values.
    @Test
    void modifyEmployee_modifyElementPosition_ShouldReturnUpdatedValue() {
        //Arrange
        EmployeeRepository employeeRepository = new EmployeeRepository();

        Employee expected = employeeRepository.modifyEmployee(2,new Employee(
                "6785530954",
                "Cole",
                "Train",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{}))));

        //Act
        Employee actual = employeeRepository.getEmployee(2);

        //Assert
        assertEquals(expected, actual);
    }

    //Deletes id number two, checks if it exists or not, if null = OK.
    @Test
    void deleteEmployee_deletesPost_ShouldReturnNull() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        employeeRepository.deleteEmployee(2);
        Employee expected = null;

        Employee actual = employeeRepository.getEmployee(2);

        assertEquals(expected, actual);
    }

    //Checks if target id exists (it does) and should be TRUE.
    @Test
    void employeeExists_checkIfIdExists_ReturnTrue() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        boolean expected = true;

        boolean actual = employeeRepository.employeeExists(1);

        assertEquals(expected, actual);

    }

    //Correct format is checked in create previously, testing invalid Luhn here. Bit redundant post-coding.
    @Test
    void isValidEmployee_createWithInvalidLuhnFormat_ShouldReturnFalse() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        boolean expected = false;

        boolean actual = employeeRepository.isValidEmployee(new Employee(
                "6785535",
                "Cole",
                "Train",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{}))));

        assertEquals(expected, actual);
    }

    @Test
    void getDevicesPerEmployee() {
    }

    @Test
    void getEmployeesLast2Years() {
    }

    @Test
    void getFullTimeBirthdays() {
    }

    //Checks if a code exists, should return true as it does exist in repo.
    @Test
    void employeeCodeExists_feedDuplicateEmployeeCode_shouldReturnTrue() {
        EmployeeRepository employeeRepository = new EmployeeRepository();

        boolean expected = true;

        boolean actual = employeeRepository.employeeCodeExists("2018269734");

        assertEquals(expected, actual);
    }
}


