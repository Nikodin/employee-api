package com.alex.employeeapi.controllers;
import com.alex.employeeapi.dataaccess.EmployeeBase;
import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.dto.DevicesPerEmployeeDTO;
import com.alex.employeeapi.models.dto.EmployeesLast2YearsDTO;
import com.alex.employeeapi.models.dto.FullTimeBirthdaysDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/employees") //base URL for entire controller.
public class EmployeeController {

    @Autowired //Injecting the repo from Employee.
    private EmployeeBase employeeRepository;

    //Get all the Employees from the map.
    @GetMapping()
    public ResponseEntity<Map<Integer, Employee>> getAllEmployees() {
        if(employeeRepository.getAllEmployees().isEmpty())
            return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.OK);
    }

    //Get specific Employee with targeted id through url.
    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id))
            return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.OK);
    }


    //Gets the latest employees from last two years, employee-code + full name as response.
    @GetMapping("/new")
    public ResponseEntity<EmployeesLast2YearsDTO> latestEmployees() {
        return new ResponseEntity<>(employeeRepository.getEmployeesLast2Years(), HttpStatus.OK);
    }


    //Return names of fulltime employees, their birthdate, and remaining days until their birthday!
    @GetMapping("/birthdays")
    public ResponseEntity<FullTimeBirthdaysDTO> fullTimeBirthdays() {
        return new ResponseEntity<>(employeeRepository.getFullTimeBirthdays(), HttpStatus.OK);
    }


    //Get Employees and Number Of devices per employees.
    @GetMapping("/devices/summary")
    public ResponseEntity<DevicesPerEmployeeDTO> devicesPerEmployee() {
        return new ResponseEntity<>(employeeRepository.getDevicesPerEmployee(), HttpStatus.OK);
    }


    //Create a new Employee by posting according to data structure, if not, bad request!
    //If structure is followed, post is created.
    @PostMapping
    public ResponseEntity<Employee>createEmployee(@RequestBody Employee employee) {
        if(employeeRepository.employeeCodeExists(employee.getEmployeeCode()))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        if (!employeeRepository.isValidEmployee(employee))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(employeeRepository.createEmployee(employee), HttpStatus.CREATED);
    }

    //Replace targeted Employee through map ID, checks whether the data format is followed, checks if the employee exists.
    //If OK, replaces. Else Error status messages.
    @PutMapping("/{id}")
    public ResponseEntity<Employee>replaceEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!employeeRepository.isValidEmployee(employee))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        if (!employeeRepository.employeeExists(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(employeeRepository.replaceEmployee(id, employee), HttpStatus.ACCEPTED);
    }

    //Updates target Employee, if null target return not found.
    //If valid, modifies the target employee post.
    @PatchMapping("/{id}")
    public ResponseEntity<Employee>modifyEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if(!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employeeRepository.modifyEmployee(id, employee), HttpStatus.OK);
    }


    //Deletes target employee with map id, through url. If the post doesn't exist error. Otherwise, deletes the post!
    @DeleteMapping("{id}")
    public ResponseEntity<String>deleteEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id)){
            return new ResponseEntity<>("ID " + id + " does not exist!", HttpStatus.NOT_FOUND);
        }

        employeeRepository.deleteEmployee(id);
        return new ResponseEntity<>("Employee with ID: " + id + " has successfully been deleted!", HttpStatus.OK);
    }
}
