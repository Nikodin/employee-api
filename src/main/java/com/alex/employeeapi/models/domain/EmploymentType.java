package com.alex.employeeapi.models.domain;

//Enum for employment types.
public enum EmploymentType {
    FULLTIME,
    PARTTIME;
}
