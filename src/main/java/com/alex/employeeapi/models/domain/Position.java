package com.alex.employeeapi.models.domain;


//Enum for the different position types used.
public enum Position {
    DEVELOPER,
    TESTER,
    MANAGER;
}

