package com.alex.employeeapi.models.domain;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Employee {
//Main constructor for Employee.
    private String employeeCode;
    private String firstName;
    private String lastName;
    private String birthDate;
    private EmploymentType employmentType;
    private String employmentDate;
    private Position position;
    private ArrayList<String> devices;

}
