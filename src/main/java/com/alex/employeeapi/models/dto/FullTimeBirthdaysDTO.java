package com.alex.employeeapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@Getter
@Setter
//ArrayList for the String results of FulTimeBirthDaysMapper.
public class FullTimeBirthdaysDTO {
    private ArrayList<String> fullTimeBirthdays;
}
