package com.alex.employeeapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;


@AllArgsConstructor
@Getter
@Setter
//ArrayList for the String results of DevicesPerEmployeeMapper.
public class DevicesPerEmployeeDTO {
    private Map<String, Integer> devicesPerEmployee;

}
