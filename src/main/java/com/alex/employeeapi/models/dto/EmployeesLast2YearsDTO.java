package com.alex.employeeapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@Getter
@Setter
//ArrayList for the String results of EmployeeLast2YearsMapper
public class EmployeesLast2YearsDTO {
    private ArrayList<String> latestEmployees;
}
