package com.alex.employeeapi.models.mappers;

import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.dto.EmployeesLast2YearsDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

//Mapper for EmployeeLast2YearsDTO
public class EmployeesLast2YearsMapper {

    public static EmployeesLast2YearsDTO mapLast2YearsEmployees(Map<Integer, Employee> employees) {

        //Creates a list, loops through the map posts.
        ArrayList<String> latest = new ArrayList<>();
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {

            //Setting values for easier read.
            var employDate = LocalDate.parse(entry.getValue().getEmploymentDate());
            var twoYearsBack = LocalDate.now().minusYears(2);

            //Check, only adds to list if the employ-date is within 2 years from today.
            if (employDate.isAfter(twoYearsBack)) {

                String l1 = entry.getValue().getFirstName();
                String l2 = entry.getValue().getLastName();
                String l3 = String.valueOf(entry.getValue().getEmployeeCode());

                latest.add("Name:  " + l1 + " " + l2);
                latest.add("Employee Code:  " + l3 );
            }
        } //Returns the result (String List) to DTO.
        return new EmployeesLast2YearsDTO(latest);
    }
}
