package com.alex.employeeapi.models.mappers;
import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.dto.DevicesPerEmployeeDTO;

import java.util.HashMap;
import java.util.Map;

//Mapper for DevicesPerEmployeeDTO
public class DevicesPerEmployeeMapper {

    public static DevicesPerEmployeeDTO mapDevicesPerEmployee(Map<Integer, Employee> employees) {

        //Creates a map, storing the first name and number of devices per entry of employees.
        Map<String, Integer> devicesPerEmployee = new HashMap<>();

        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            var fullName = entry.getValue().getFirstName() + " " + entry.getValue().getLastName();
            var amountOfDevices = entry.getValue().getDevices().size();
            devicesPerEmployee.put(fullName, amountOfDevices);

        }
        return new DevicesPerEmployeeDTO(devicesPerEmployee);
    }
}
