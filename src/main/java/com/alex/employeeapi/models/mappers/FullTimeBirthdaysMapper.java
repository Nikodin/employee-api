package com.alex.employeeapi.models.mappers;
import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.domain.EmploymentType;
import com.alex.employeeapi.models.dto.FullTimeBirthdaysDTO;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Map;

//Mapper for FullTimeBirthdaysDTO
public class FullTimeBirthdaysMapper {

    public static FullTimeBirthdaysDTO mapFullTimeBirthdays(Map<Integer, Employee> employees) {

        //Creates a String list, loops through the map posts, adds the first/last-names and the sum of the entries.
        ArrayList<String> birthdayList = new ArrayList<>();

        //Iterates through entries in map, Employee posts.
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {

            //Setting values for easier read/use.
            var fullTime = entry.getValue().getEmploymentType();
            var birthDay = LocalDate.parse(entry.getValue().getBirthDate());
            var today = LocalDate.now();

            long days = daysUntilBirthday(birthDay, today);

        //Checks if employee is Full-time before adding values onto the list.
            if (fullTime.equals(EmploymentType.FULLTIME)) {

                String l1 = entry.getValue().getFirstName();
                String l2 = entry.getValue().getLastName();

                birthdayList.add("Name:  " + l1 + " " + l2);
                birthdayList.add("Birthday:  " + birthDay );

                //Counts days left until birthday, else notifies it is his/hers birthday!
                if (days != 365)
                    birthdayList.add(days + " Days left until birthday!");
                else
                birthdayList.add("IT'S HIS/HERS BIRTHDAY TODAY!");
            }
        } // Returns the list to the DTO.
        return new FullTimeBirthdaysDTO(birthdayList);
    }

    //First gets the diff in years since birth as years.
    //Then gets the days between today, and the sum of years passed + one = as days!
    public static long daysUntilBirthday(LocalDate birthDay, LocalDate today) {
        var yearsPassed = ChronoUnit.YEARS.between(birthDay, today);

        return ChronoUnit.DAYS.between(today, birthDay.plusYears(yearsPassed+1));
    }
}







