package com.alex.employeeapi.dataaccess;
import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.domain.EmploymentType;
import com.alex.employeeapi.models.domain.Position;
import com.alex.employeeapi.models.dto.DevicesPerEmployeeDTO;
import com.alex.employeeapi.models.dto.EmployeesLast2YearsDTO;
import com.alex.employeeapi.models.dto.FullTimeBirthdaysDTO;
import com.alex.employeeapi.models.mappers.DevicesPerEmployeeMapper;
import com.alex.employeeapi.models.mappers.EmployeesLast2YearsMapper;
import com.alex.employeeapi.models.mappers.FullTimeBirthdaysMapper;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EmployeeRepository implements EmployeeBase {

    //Feeds  Map with dummies through method.
    private Map<Integer, Employee> employees = seedEmployees();

    //Generates an in memory dummie of posts.
    private Map<Integer, Employee> seedEmployees() {
        var employees = new HashMap<Integer, Employee>();

        employees.put(1, new Employee(
                "2018269734",
                "John",
                "Doe",
                "1980-02-14",
                EmploymentType.PARTTIME,
                "2021-03-31",
                Position.TESTER,
                new ArrayList<>(List.of(new String[]{"DELL XPS 13"}))));
        employees.put(2, new Employee(
                "1634751505",
                "Jane",
                "Doe",
                "1984-05-17",
                EmploymentType.FULLTIME,
                "2018-12-01",
                Position.MANAGER,
                new ArrayList<>(List.of(new String[]{"Alienware X17", "Samsung 30-inch LED monitor"}))));
        employees.put(3, new Employee(
                "1507119608",
                "Sven",
                "Svensson",
                "1987-03-22",
                EmploymentType.FULLTIME,
                "2019-02-15",
                Position.DEVELOPER,
                new ArrayList<>(List.of(new String[]{"DELL XPS 15", "Samsung 27-inch LED Monitor"}))));

        return employees;
    }

    //Returns the entire pool of posts from the employees hashmap.
    @Override
    public Map<Integer, Employee> getAllEmployees() {
        return employees;
    }

    //Returns a specific Employee by targeting id.
    @Override
    public Employee getEmployee(int id) {
        return employees.get(id);
    }

    //Creates a new Employee post, with an incremented key as new, returns the new post.
    @Override
    public Employee createEmployee(Employee employee) {
        Integer maxId = Collections.max(employees.keySet());
        employees.put(maxId + 1, employee);
        return employees.get(maxId + 1);
    }

    //Replaces targeted Employee with id, and returns the replacement.
    @Override
    public Employee replaceEmployee(int id, Employee employee) {
        employees.replace(id, employee);
        return employees.get(id);
    }

    //Modifies an Employee post using the constructor values, returns modified employee.
    @Override
    public Employee modifyEmployee(int id, Employee employee) {
        var employeeToModify = getEmployee(id);
        employeeToModify.setEmployeeCode(employee.getEmployeeCode());
        employeeToModify.setFirstName(employee.getFirstName());
        employeeToModify.setLastName(employee.getLastName());
        employeeToModify.setBirthDate(employee.getBirthDate());
        employeeToModify.setEmploymentType(employee.getEmploymentType());
        employeeToModify.setEmploymentDate(employee.getEmploymentDate());
        employeeToModify.setPosition(employee.getPosition());
        employeeToModify.setDevices(employee.getDevices());

        return employeeToModify;
    }

    //Remove employee post with targeted id.
    @Override
    public void deleteEmployee(int id) {
        employees.remove(id);
    }

    //Checks whether the employee exists or not through map id.
    @Override
    public boolean employeeExists(int id) {
        return getEmployee(id) != null;
    }

    //Checks if the Employee post follows the demanded data format incl validation, if any is empty = false.
    @Override
    public boolean isValidEmployee(Employee employee) {
        return
                employee.getEmployeeCode() != null &&
                        LuhnValidator.employeeCodeValidator(employee.getEmployeeCode()) &&
                        employee.getFirstName() != null &&
                        employee.getLastName() != null &&
                        employee.getBirthDate() != null &&
                        employee.getEmploymentType() != null &&
                        employee.getEmploymentDate() != null &&
                        employee.getPosition() != null &&
                        employee.getDevices() != null;
    }

    //Gets devices per employee as a String, returns the new String List.
    @Override
    public DevicesPerEmployeeDTO getDevicesPerEmployee() {
        return DevicesPerEmployeeMapper.mapDevicesPerEmployee(employees);
    }

    //Gets the newest (last 2 years) employees as a String List.
    @Override
    public EmployeesLast2YearsDTO getEmployeesLast2Years() {
        return EmployeesLast2YearsMapper.mapLast2YearsEmployees(employees);
    }

    //Gets the birthdays of full-time employees, as a String List.
    @Override
    public FullTimeBirthdaysDTO getFullTimeBirthdays() {
        return FullTimeBirthdaysMapper.mapFullTimeBirthdays(employees);
    }

    //Checks if another employee shares the same code.
    @Override
    public boolean employeeCodeExists(String code) {
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
           if (code.equals(entry.getValue().getEmployeeCode())) {
               return true;
            }
        }
        return false;
    }
}





