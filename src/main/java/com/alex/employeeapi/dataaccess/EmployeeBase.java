package com.alex.employeeapi.dataaccess;

import com.alex.employeeapi.models.domain.Employee;
import com.alex.employeeapi.models.dto.DevicesPerEmployeeDTO;
import com.alex.employeeapi.models.dto.EmployeesLast2YearsDTO;
import com.alex.employeeapi.models.dto.FullTimeBirthdaysDTO;

import java.util.Map;

public interface EmployeeBase {

    //Base Crud
    Map<Integer, Employee> getAllEmployees();
    Employee getEmployee (int id);
    Employee createEmployee(Employee employee);
    Employee replaceEmployee(int id, Employee employee);
    Employee modifyEmployee(int id, Employee employee);
    void deleteEmployee(int id);
    boolean employeeExists(int id);
    boolean isValidEmployee(Employee employee);
    boolean employeeCodeExists(String code);

    //DTO methods.
    DevicesPerEmployeeDTO getDevicesPerEmployee();
    EmployeesLast2YearsDTO getEmployeesLast2Years();
    FullTimeBirthdaysDTO getFullTimeBirthdays();


}
