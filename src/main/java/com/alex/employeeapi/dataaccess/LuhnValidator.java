package com.alex.employeeapi.dataaccess;

public class LuhnValidator {

    //Validates the input string/generated Luhn-code.
    public static boolean employeeCodeValidator(String code) {

        //Checks if the code is anything else than demand, 10 digits!
        if (code.length() != 10) {
            return false;
        }
        //Separate methods for both cases of the algorithm. Stores the value of these methods to be used
        //for the last check!
        int evenSum = evenNumbersCount(code);
        int oddSum = oddNumbersCount(code);

        //If the rules are followed returns true!
        return (evenSum + oddSum) % 10 == 0;
    }

    //Checks the even entries one by one, follows the rules of the Luhn-algorithm and
    //returns the sum of every even entry.
    protected static int evenNumbersCount(String code) {
        int codeLength = code.length();
        int evenSum = 0;

        for (int i = codeLength - 2; i >= 0; i = i - 2) {
            int n = code.charAt(i) - '0';

            int n2 = n * 2;

            if (n2 > 9) {
                n2 = n2 % 10 + n2 / 10;
            }

            evenSum += n2;
        }
        return evenSum;
    }

    //Checks every odd entry of the code, follows the rules of the algorithm for odd specifically.
    //Returns the sum of every odd entry.
    protected static int oddNumbersCount(String code) {
        int codeLength = code.length();
        int oddSum = 0;

        for (int i = codeLength - 1; i >= 1; i = i - 2) {
            int n = code.charAt(i) - '0';

            oddSum += n;
        }

        return oddSum;
    }
}


