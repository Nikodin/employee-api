# Update History (Employee API)

**With Every Update, a commit is made**

### 4/12/2022 4:52 PM - Tried out Map for one of the mappers / Closing thoughts
Tested to use a map for the DevicesPerEmployeeMapper, it was easy to get done but I'm not  
happy about it. I remember "you" mentioned that I could just change the constructor for the DTO  
to only have the values as fields, when I tried that out it asked to be newed up every time  
in the mapper, which in turn erased every previous entry that was created throughout the loop. 

I'm sure I'm not getting it the right way haha! I guess it can serve as an example that I can do it another way  
rather than stringyfing everything into String lists for no reason (as the other DTO's). 

**Closing thoughts:**  
I'm not entirely happy as I see so many implementations/upgrades for different cases as stated  
previously. However for the deadline I feel maxxed out, spent huge amount of time on this and it felt  
really good, for that reason I am proud. Tons of fun (and pain).

In time I hope to return to this and laugh.

The product that I ship however, I always want more!

PS. I got it up but not entirely!

### 4/11/2022 5:09 PM - TDD + Fixes + Research
Added a check for duplicate employee-codes, I do see the pattern and the holes being present throughout  
the code now, as I haven't countered every scenario. With the time I had to put in, it seems very far away for me  
to get it done for the due to date. This would've been a scenario which could've been found using TDD for sure.

Adjusted birthday message in mapper. 

Researched patchings, but it seems you have to hard code the scenarios one by one, keeping the info you  
don't change. 

Implementing these changes however, has also made me seen how tightly coupled some of these methods are  
in some controllers, adding the duplicate code check for example will break my current patch as it doesn't cater for 
saving previous information etc as stated above. 

When looking around the code there are sooo many possibilities for improvement but I'm not on that output  
level yet!

### 4/10/2022 6:32 PM - Repo TDD 
Continued with the tests. 

It does feel redundant when I did (my dumb mistake) a lot of previous tests through requests in postman regarding much  
of the repo. Since I feared the pressure/time due to previous missions I wanted to build the crud with that experience in mind.  

Sadly it does affect the perspective of TDD. I'm glad I did follow through with the correct path on some of the elements, oh well. 

With some time left now before the due date I will take a look at making the mappers better. I might learn a thing or two from it. 

Other than that, I feel V1 is sort of done. And I am very proud I got this far despite the poop. 

Possible TODO's:  
-- Change the way data is stored in mappers.  
-- Add date formatter.  
-- Add TDD for Mappers, especially if they are redesigned as this allows for the correct way of TDD.  
-- Separate requests for devices ONLY.

### 4/10/2022 2:45 PM - Repo TDD + Heroku/Docker
Building the last TDD for the repo, struggled with hash comparisons not giving the green light. 

Asked a peer for aid and got the solution which I applied to the Employee class @EqualsAndHashCode.

Plan is to continue to fill in with the last repo tests, post-coding sadly... Facepalm.

Regarding the thoughts I will try to get an alternative storage to work for the DTO's, teachers input was not positive as we  
agreed on the fails with having the data stored as entire strings and not values themselves. 

Also I'll try to get a working request/patch for devices only per individual.

Docker instance is up and running, on Heroku as well so at least that's up and OK. Had some issues with the wrong version  
being present in the pom.xml, changed it to 17 and it worked. Took some research. I know the culprit is the Lomboks. 

I'm da web dev now! 


### 4/6/2022 4:26 PM - Cleanup and docs
Cleaned up comments, added comments, and tried to "style" the current code. Structured folders against previous  
examples.

Renamed updates to worklog, as it should be... Updated the readme with the new requests. 

Post-coding I now have to create a test for the repo (which I know is not the proper way)

Thoughts:  
-- I consider the String List responses throughout my DTO's to be problematic considering the data within is  
a lot harder to reach now (very).  
-- Maybe implementing a converter pattern for the input dates, for both DTO's.  
-- Feed the demanded repo (appendix examples) before being done.  
-- Before due date, test the functionality through Postman again. 


### 4/5/2022 8:14 PM - DTO for FullTimeBirthdays DONE!
I can't say this enough, blood sweat, tears, screams! Through fire and flames! 

It works... 

The logic seems to work, I'll continue to test different scenarios tomorrow. I'll update logs/comments with info  
regarding the named DTO. 

I am fried, time to flee. 


### 4/5/2022 3:58 PM - DTO for latest employees works!
Built the logic for named DTO, and it works, tested through postman!

Added criteria for Luhn Validator, 10 digits exactly ONLY!   
-- Tests made for the check, sadly post-coding. 

Started with the last DTO "birthdays".


### 4/4/2022 1:08 PM - Prepping DTO's
Created Mappers and DTO's for the two remaining planned implementations, one being  
the Birthdays, and the other the 2 year latest employees. Logic is not completed.

Controllers/Mappers communication has been tested with a previous logic.

### 4/4/2022 10:46 PM - First DTO (Devices Summary)
Spend quite some time trying several methods out to retrieve and create the proper storage 
for the devices summary. I managed to finally get an arrayList to work...

If it is OK, the first DTO is completed since the requests works!

### 4/4/2022 7:06 PM - I MADE AN ARRAY THROUGH CONSTRUCT!
I was finally able to create the thing I've wanted for HOURS, spent hours googling for the possible syntax. 

When a peer tried a similar solut### 4/4/2022 10:46 PM - First DTO (Devices Summary)ion, I spent some time with it yet again and found it! This will help for the
DTO's.

### 4/4/2022 5:38 PM - Implementing validator in controllers. 
Adding the validator for the employeeCode in validEmployee which affects Post/Put  
controllers, possibly patch? As it is not demanded... hmm.

Tested the controllers with the validator implemented, the responses meet my demands!

Now to the first DTO, still haven't decided which one.

### 4/3/2022 7:31 PM - Luhn + TDD (COMPLETED!)
Luhn-validation method completed in a separate class, created two separate sub-methods within  
to force myself to really break the algo down. 

Created tests for both the odd numbers, and even numbers results following the algorithm. Tested and  
completed! 

-- TDD for Luhn completed (pre coding!).   
-- Luhh method completed.

Next mission is to implement it in the affected controllers. 

Honorable mentions, Jerry joined during the debugging! 

### 4/3/2022 3:22 PM - Luhn + TDD
Tinkering with Luhn-validator, using the TDD approach and constructing the test first. Already noticed  
that I can't use a long... possibly have to use a String as the foundation data. 

Then I might be able to iterate through the String. The method used will be a boolean,  
as it is a validation method. Plan is to put it into isValidEmployee() as the first step  
during post/patch etc. 

### 4/3/2022 12:18 PM - Updated readme "how it works", research. 
Updated readme now including the functionality of the controllers created previously, and how to use them   
through postman.  

Researched how to store certain data in the constructor.  

Next is to build one of the DTO's and possibly start with the Luhn algo id problem.

### 4/2/2022 9:56 PM - Created controllers for the base/checked.
Created and tested controllers for the basic functionality of the application through Postman. 

Tested through Postman:  
-- Get All Employees.  
-- Get a specific employee by ID through url.  
-- Post a new Employee.  
-- Replace an existing Employee.  
-- Update an existing Employee.   
-- Delete an existing Employee. 
 
Every request has been error checked in terms of wrong input, non existing post etc.  
Added comments to most of the controllers. 

*PS. I am dead time to sleep. Plan for tomorrow:   
-- Update readme with the current implementations.  
-- Target a specific problem.  
-- Work on at least one of the DTO's.*



### 4/2/2022 6:35 PM - Working on filestructure and base. 
Created packages and javas following the pattern/structure of the previous  
guitar project. Written the repos, enums, and researched about how to construct different values -  
specifically the Dates used, and of course the devices value within the object. 

Going for String for now with dates,we'll see if the plan goes as I'd hoped later. Haven't decided  
how to implement the unique generated key yet, to use as key in map, or as a  subvalue of object. 



-- Created Repos   
-- Created packages for the different classes/methods.   
-- Created Interface for the repos.  
-- Filled in comments for specific methods  
-- Research...  

*Working on the Controllers, and testing the links/methods from Postman next update.*





### 4/1/2022 1:40 PM - Created Git Repository/Project   
To be able to easily track and share the project with peers/teacher as I progress. 
